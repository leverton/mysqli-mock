<?php

class MysqliMock extends AbstractMysqli
{
    private $character_set_name = 'latin1';

    public function autocommit(bool $enable) : bool
    {
        return true;
    }

    public function begin_transaction(int $flags = 0, ?string $name = null): bool
    {
        return true;
    }

    public function change_user(string $username, string $password, ?string $database): bool
    {
        return true;
    }

    public function character_set_name(): string
    {
        return $this->character_set_name;
    }

    public function close(): bool
    {
        return true;
    }

    public function commit(int $flags = 0, ?string $name = null): bool
    {
        return true;
    }

    public function debug(string $options): bool
    {
        return true;
    }

    public function dump_debug_info(): bool
    {
        return true;
    }

    public function get_charset(): ?object
    {
        return (object) [
            'charset' => $this->character_set_name,
            'collation' => 'latin1_swedish_ci',
            'dir' => '',
            'min_length' => 1,
            'max_length' => 1,
            'number' => 8,
            'state' => 801,
        ];
    }

    public function get_connection_stats(): array
    {
        return [];
    }

    public function get_warnings(): AbstractMysqliWarning|false
    {
        return false;
    }

    public function kill(int $process_id): bool
    {
        return true;
    }

    public function more_results(): bool
    {
        return false;
    }

    public function multi_query(string $query): bool
    {
        return true;
    }

    public function next_result(): bool
    {
        return true;
    }

    public function options(int $option, string|int $value): bool
    {
        return true;
    }

    public function ping(): bool
    {
        return true;
    }

    static public function poll(?array &$read, ?array &$error, array &$reject, int $seconds, int $microseconds = 0): int|false
    {
        return false;
    }

    public function prepare(string $query): AbstractMysqliStmt|false
    {
        return false;
    }

    public function query(string $query, int $result_mode = MYSQLI_STORE_RESULT): AbstractMysqliResult|bool
    {
        return false;
    }

    public function real_connect(string $host = '', string $username = '', string $passwd = '', string $dbname = '', int $port = 0, string $socket = '', int $flags = 0): bool
    {
        return true;
    }

    public function real_escape_string(string $string): string
    {
        return $string;
    }

    public function real_query(string $query): bool
    {
        return true;
    }

    public function reap_async_query(): AbstractMysqliResult|bool
    {
        return false;
    }

    public function refresh(int $flags): bool
    {
        return true;
    }

    public function release_savepoint(string $name): bool
    {
        return true;
    }

    public function rollback(int $flags = 0, ?string $name = null): bool
    {
        return true;
    }

    public function savepoint(string $name): bool
    {
        return true;
    }

    public function select_db(string $database): bool
    {
        return true;
    }

    public function set_charset(string $charset): bool
    {
        $this->character_set_name = $charset;
        return true;
    }

    public function ssl_set(?string $key, ?string $certificate, ?string $ca_certificate, ?string $ca_path, ?string $cipher_algos): bool
    {
        return true;
    }

    public function stat(): string|false
    {
        return false;
    }

    public function stmt_init(): AbstractMysqliStmt|false
    {
        return false;
    }

    public function store_result(int $mode = 0): AbstractMysqliResult|false
    {
        return false;
    }

    public function thread_safe(): bool
    {
        return false;
    }

    public function use_result(): AbstractMysqliResult|false
    {
        return false;
    }
}