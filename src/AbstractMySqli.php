<?php

abstract class AbstractMysqli
{
    public readonly int|string $affected_rows;
    public readonly int $connect_errno;
    public readonly ?string $connect_error;
    public readonly int $errno;
    public readonly array $error_list;
    public readonly string $error;
    public readonly int $field_count;
    public readonly string $client_info;
    public readonly int $client_version;
    public readonly string $host_info;
    public readonly int $protocol_version;
    public readonly string $server_info;
    public readonly int $server_version;
    public readonly ?string $info;
    public readonly int|string $insert_id;
    public readonly string $sqlstate;
    public readonly int $thread_id;
    public readonly int $warning_count;

    abstract public function autocommit(bool $enable): bool;
    abstract public function begin_transaction(int $flags = 0, ?string $name = null): bool;
    abstract public function change_user(string $username, string $password, ?string $database): bool;
    abstract public function character_set_name(): string;
    abstract public function close(): bool;
    abstract public function commit(int $flags = 0, ?string $name = null): bool;
    abstract public function debug(string $options): bool;
    abstract public function dump_debug_info(): bool;
    abstract public function get_charset(): ?object;
    abstract public function get_connection_stats(): array;
    abstract public function get_warnings(): AbstractMysqliWarning|false;
    abstract public function kill(int $process_id): bool;
    abstract public function more_results(): bool;
    abstract public function multi_query(string $query): bool;
    abstract public function next_result(): bool;
    abstract public function options(int $option, string|int $value): bool;
    abstract public function ping(): bool;
    abstract static public function poll(?array &$read, ?array &$error, array &$reject, int $seconds, int $microseconds = 0): int|false;
    abstract public function prepare(string $query): AbstractMysqliStmt|false;
    abstract public function query(string $query, int $result_mode = MYSQLI_STORE_RESULT): AbstractMysqliResult|bool;
    abstract public function real_connect(string $host = '', string $username = '', string $passwd = '', string $dbname = '', int $port = 0, string $socket = '', int $flags = 0): bool;
    abstract public function real_escape_string(string $string): string;
    abstract public function real_query(string $query): bool;
    abstract public function reap_async_query(): AbstractMysqliResult|bool;
    abstract public function refresh(int $flags): bool;
    abstract public function release_savepoint(string $name): bool;
    abstract public function rollback(int $flags = 0, ?string $name = null): bool;
    abstract public function savepoint(string $name): bool;
    abstract public function select_db(string $database): bool;
    abstract public function set_charset(string $charset): bool;
    abstract public function ssl_set(?string $key, ?string $certificate, ?string $ca_certificate, ?string $ca_path, ?string $cipher_algos): bool;
    abstract public function stat(): string|false;
    abstract public function stmt_init(): AbstractMysqliStmt|false;
    abstract public function store_result(int $mode = 0): AbstractMysqliResult|false;
    abstract public function thread_safe(): bool;
    abstract public function use_result(): AbstractMysqliResult|false;
}
